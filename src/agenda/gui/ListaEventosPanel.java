package agenda.gui;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import agenda.io.AgendaIO;

public class ListaEventosPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTable table;
	
	public ListaEventosPanel() {
		setLayout(new BorderLayout(0, 0));
		
		table = new JTable(getDataColumns(), getNameColumns());
		JScrollPane scroll = new JScrollPane(table);
		add(scroll, BorderLayout.CENTER);
	}
	
	/**
	 * Adiciona uma nova linha para a tabela
	 * @param Object[] valores a serem inclu�dos na linha
	 */
	public void addNewRow(Object[] valores){
		((DefaultTableModel)table.getModel()).addRow(valores);
	}
	
	/**
	 * Retorna o nome das colunas da tabela
	 * @return Vector<String> contendo o nome das colunas
	 */
	private Vector<String> getNameColumns(){
		Vector<String> nameColumns = new Vector<String>();
		nameColumns.add("Data");
		nameColumns.add("Descri��o");
		nameColumns.add("Periodicidade");
		nameColumns.add("E-mail");
		nameColumns.add("Alarme");
		return nameColumns;
	}
	
	/**
	 * Retorna os dados das colunas
	 * @return Vector<Vector<Object>> contendo os dados das colunas
	 */
	private Vector<Vector<Object>> getDataColumns(){
		AgendaIO io = new AgendaIO();
		Vector<Vector<Object>> dataColumns = null;
		
		try {
			dataColumns = io.getEventos();
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		return dataColumns;
	}
}