package agenda.gui;

import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

import agenda.io.AgendaIO;
import agenda.utils.AgendaUtils;
import agenda.utils.PeriodicidadeEnum;
import agenda.vo.Evento;
import javax.swing.JButton;

public class CadastroEventoPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private JTextField tfDtEvento;
	private JTextField tfEmailEvento;
	private JTextField tfDescEvento;
	private JRadioButton rdbtnUmaVez;
	private JRadioButton rdbtnSemanal;
	private JRadioButton rdbtnMensal;
	private JCheckBox chckbxAlarme;
	private ListaEventosPanel listaEventos;

	public CadastroEventoPanel(ListaEventosPanel listaEventos) {
		this.listaEventos = listaEventos;
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Descri��o do Evento");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(10, 11, 128, 14);
		add(lblNewLabel);
		
		JLabel lblDataDoEvento = new JLabel("Data do Evento");
		lblDataDoEvento.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDataDoEvento.setBounds(10, 74, 93, 14);
		add(lblDataDoEvento);
		
		tfDtEvento = new JTextField();
		tfDtEvento.setBounds(126, 71, 128, 20);
		add(tfDtEvento);
		tfDtEvento.setColumns(10);
		
		JLabel lblPeriodicidadeDoEvento = new JLabel("Periodicidade do Evento");
		lblPeriodicidadeDoEvento.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPeriodicidadeDoEvento.setBounds(9, 149, 150, 14);
		add(lblPeriodicidadeDoEvento);
		
		rdbtnUmaVez = new JRadioButton("Uma vez");
		rdbtnUmaVez.setSelected(true);
		rdbtnUmaVez.setBounds(157, 145, 89, 23);
		add(rdbtnUmaVez);
		
		rdbtnSemanal = new JRadioButton("Semanal");
		rdbtnSemanal.setBounds(248, 145, 94, 23);
		add(rdbtnSemanal);
		
		rdbtnMensal = new JRadioButton("Mensal");
		rdbtnMensal.setBounds(344, 145, 72, 23);
		add(rdbtnMensal);
		
		JLabel lblEncaminharEmail = new JLabel("Encaminhar e-mail");
		lblEncaminharEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEncaminharEmail.setBounds(10, 112, 112, 14);
		add(lblEncaminharEmail);
		
		tfEmailEvento = new JTextField();
		tfEmailEvento.setBounds(126, 109, 202, 20);
		add(tfEmailEvento);
		tfEmailEvento.setColumns(10);
		
		tfDescEvento = new JTextField();
		tfDescEvento.setBounds(10, 34, 401, 20);
		add(tfDescEvento);
		tfDescEvento.setColumns(10);
		
		ButtonGroup btnGroup = new ButtonGroup();
		btnGroup.add(rdbtnUmaVez);
		btnGroup.add(rdbtnSemanal);
		btnGroup.add(rdbtnMensal);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(126, 185, 89, 23);
		add(btnSalvar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.setBounds(272, 185, 89, 23);
		add(btnLimpar);
		
		chckbxAlarme = new JCheckBox("Alarme");
		chckbxAlarme.setFont(new Font("Tahoma", Font.BOLD, 11));
		chckbxAlarme.setBounds(6, 185, 97, 23);
		add(chckbxAlarme);
		
		btnSalvar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				chamaCadastroEvento();
				
			}
		});
		
		btnLimpar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				limparCampos();
				
			}
		});
	}
	
	/**
	 * Constr�i um objeto do tipo Evento com as informa��es da tela
	 * e chama a rotina para cadastramento do mesmo
	 */
	private void chamaCadastroEvento(){
		AgendaIO io = new AgendaIO();
		Evento evento = new Evento();
		
		Object[] novaLinha = new Object[5];
		
		evento.setDataEvento(AgendaUtils.getDateFromString(tfDtEvento.getText()));
		evento.setDescEvento(tfDescEvento.getText());
		evento.setAlarme(chckbxAlarme.isSelected() ? 1 : 0);
		evento.setEmailEncaminhar(tfEmailEvento.getText());
		
		novaLinha[0] = tfDtEvento.getText();
		novaLinha[1] = tfDescEvento.getText();
		novaLinha[4] = chckbxAlarme.isSelected() ? "LIGADO" : "DESLIGADO";
		novaLinha[3] = tfEmailEvento.getText();
				
		if(rdbtnUmaVez.isSelected()){
			evento.setPeriodicidade(PeriodicidadeEnum.UNICO);
			novaLinha[2] = PeriodicidadeEnum.UNICO;
		}
		else if(rdbtnSemanal.isSelected()){
			evento.setPeriodicidade(PeriodicidadeEnum.SEMANAL);
			novaLinha[2] = PeriodicidadeEnum.SEMANAL;
		}
		else {
			evento.setPeriodicidade(PeriodicidadeEnum.MENSAL);
			novaLinha[2] = PeriodicidadeEnum.MENSAL;
		}
		
		try {
			io.gravarEvento(evento);
			
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null, "ERRO", ex.getMessage(), JOptionPane.ERROR_MESSAGE);
		}
		listaEventos.addNewRow(novaLinha);
		limparCampos();
		
	}
	
	/**
	 * Realiza o reset dos campos de cadastro da tela.
	 */
	private void limparCampos(){
		tfDtEvento.setText("");
		tfDescEvento.setText("");
		chckbxAlarme.setSelected(false);
		tfEmailEvento.setText("");
		rdbtnUmaVez.setSelected(true);
	}
}
