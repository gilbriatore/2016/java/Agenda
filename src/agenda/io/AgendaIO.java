package agenda.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Vector;

import agenda.utils.PeriodicidadeEnum;
import agenda.vo.Evento;

/**
 * Classe respons�vel por ler e gravar informa��es de eventos em um
 * arquivo texto
 * @author Renato G. Oliveira
 */
public class AgendaIO {
	/**
	 * Representa o diret�rio onde ser� gravado o arquivo
	 */
	private final String DIRETORIO = ".";
	
	/**
	 * Nome do arquivo de eventos
	 */
	private final String ARQUIVO = "eventos.txt";
	
	/**
	 * Realiza a grava��o de um evento no arquivo da agenda
	 * @param evento Cont�m as informa��es do evento a ser gravado no arquivo
	 * @throws Exception Caso o arquivo n�o seja encontrado ou ocorra
	 * algum problema na grava��o do evento no arquivo
	 */
	public void gravarEvento(Evento evento)throws Exception{
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(DIRETORIO, ARQUIVO), true));
			writer.write(evento.toString(), 0, evento.toString().length());
			writer.newLine();
			writer.close();
			
		}catch(FileNotFoundException fnfe){
			throw new Exception("Arquivo n�o encontrado");
			
		}catch(Exception ex){
			throw new Exception("Problemas na grava��o do arquivo");
			
		}
	}
	
	/**
	 * Retorna uma lista de eventos cadastrados no arquivo
	 * @return List<Evento> Contendo a lista de eventos do arquivo
	 * @throws Exception Caso ocorra alguma problema na localiza��o do arquivo ou na leitura do mesmo
	 */
	public Vector<Vector<Object>> getEventos()throws Exception{
		Vector<Vector<Object>> lista = new Vector<Vector<Object>>();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(DIRETORIO, ARQUIVO)));
			String linha = reader.readLine();
			while(linha != null){
				Vector<Object> eventoVector = new Vector<Object>();
				String[] tokens = linha.split(";");

				eventoVector.add(tokens[0]);
				eventoVector.add(tokens[1]);
				eventoVector.add(PeriodicidadeEnum.valueOf(tokens[2]));
				eventoVector.add(tokens[3]);
				eventoVector.add(tokens[4].equals("1") ? "LIGADO" : "DESLIGADO");
				
				lista.add(eventoVector);
				linha = reader.readLine();
			}
			
			reader.close();
			
		}catch(FileNotFoundException fnfe){
			throw new Exception("Arquivo n�o encontrado");
			
		}catch(Exception ex){
			throw new Exception("Problemas de leitura no arquivo de eventos");
			
		}
		return lista;
	}
}
