package agenda.vo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import agenda.utils.PeriodicidadeEnum;

/**
 * Representa um evento da Agenda
 * @author Renato G. Oliveira
 */
public class Evento implements Serializable {
	/**
	 * Identificador da classe
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Data do evento
	 */
	private Date dataEvento;
	
	/**
	 * Descri��o do evento
	 */
	private String descEvento;
	
	/**
	 * Periodicidade de repeti��o do evento
	 */
	private PeriodicidadeEnum periodicidade;
	
	/**
	 * Email a ser encaminhado o evento
	 */
	private String emailEncaminhar;
	
	/**
	 * Alarme do evento
	 */
	private int alarme;
	
	/**
	 * Construtor default da classe
	 */
	public Evento() {
		super();
	}
	
	/**
	 * Construtor que recebe todos os par�metros da classe
	 * @param dataEvento
	 * @param descEvento
	 * @param periodicidade
	 * @param emailEncaminhar
	 * @param alarme
	 */
	public Evento(Date dataEvento, String descEvento,
			PeriodicidadeEnum periodicidade, String emailEncaminhar, int alarme) {
		super();
		this.dataEvento = dataEvento;
		this.descEvento = descEvento;
		this.periodicidade = periodicidade;
		this.emailEncaminhar = emailEncaminhar;
		this.alarme = alarme;
	}
	
	/**
	 * Retorna a data do evento
	 * @return Date contento a data do evento
	 */
	public Date getDataEvento() {
		return dataEvento;
	}
	
	/**
	 * Altera a data do evento
	 * @param Date contento a nova data do evento
	 */
	public void setDataEvento(Date dataEvento) {
		this.dataEvento = dataEvento;
	}
	
	/**
	 * Retorna a descri��o do evento
	 * @return String contento a descri��o do evento
	 */
	public String getDescEvento() {
		return descEvento;
	}
	
	/**
	 * Altera a descri��o do evento
	 * @param String contento a nova descri��o do evento
	 */
	public void setDescEvento(String descEvento) {
		this.descEvento = descEvento;
	}
	
	/**
	 * Retorna a periodicidade do evento
	 * @return PeriodicidadeEnum contento a periodicidade do evento
	 */
	public PeriodicidadeEnum getPeriodicidade() {
		return periodicidade;
	}
	
	/**
	 * Altera a periodicidade do evento
	 * @param PeriodicidadeEnum contento a nova periodicidade do evento
	 */
	public void setPeriodicidade(PeriodicidadeEnum periodicidade) {
		this.periodicidade = periodicidade;
	}
	
	/**
	 * Retorna o e-mail de aviso do evento
	 * @return String contento o e-mail a ser enviado do evento
	 */
	public String getEmailEncaminhar() {
		return emailEncaminhar;
	}
	
	/**
	 * Altera o e-mail a ser avisado sobre o evento
	 * @param String contento o novo e-mail
	 */
	public void setEmailEncaminhar(String emailEncaminhar) {
		this.emailEncaminhar = emailEncaminhar;
	}
	
	/**
	 * Retorna o alarme de aviso do evento
	 * @return int contento o n�mero em segundos do evento
	 */
	public int getAlarme() {
		return alarme;
	}
	
	/**
	 * Altera o alarme do evento
	 * @param int contento o novo alarme do evento
	 */
	public void setAlarme(int alarme) {
		this.alarme = alarme;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		SimpleDateFormat mascaraData = new SimpleDateFormat("dd/MM/yyyy");
		return (mascaraData.format(getDataEvento()) + ";" + getDescEvento() + ";" + getPeriodicidade() + ";" + getEmailEncaminhar() + ";" + getAlarme());
	}
}
