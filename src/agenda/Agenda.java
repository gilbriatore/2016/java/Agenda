package agenda;

import agenda.gui.AgendaFrame;

/**
 * Classe respons�vel por iniciar a tela principal da agenda
 * @author Renato Gon�alves de Oliveira
 */
public class Agenda {

	/**
	 * Realiza uma chamada para iniciar a tela principal
	 * @param args
	 */
	public static void main(String[] args) {
		new Agenda().iniciarTela();

	}
	
	/**
	 * Cria uma inst�ncia do frame e o torna vis�vel
	 */
	private void iniciarTela(){
		AgendaFrame frame = new AgendaFrame();
		frame.setVisible(true);
	}
}